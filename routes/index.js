const express = require("express");
const router = express.Router();
import room_router from "./category";
import user_router from "./product";

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Welcome were wolf BE!" });
});

export { router, room_router, user_router };
