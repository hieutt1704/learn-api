const express = require("express");
const router = express.Router();

let categories = [
  { ID: 0, name: "Quần áo" },
  { ID: 1, name: "Sách" },
  { ID: 2, name: "Món ăn" },
  { ID: 3, name: "Hoa quả" },
  { ID: 4, name: "Giày dép" },
];

router.get("/list", async (request, response) => {
  response.json({ status: true, categories });
});

router.post("/", async (request, response) => {
  const { name } = request.body;
  if (name) {
    const ID = categories.length;
    categories = [...categories, { ID, name }];
    response.json({ status: true });
  } else {
    response.json({ status: false, error: "Thiêu name" });
  }
});

router.get("/", (request, response) => {
  const { ID } = request.query;
  if (ID) {
    const category = categories.find((cate) => cate.ID == ID);
    if (category) {
      response.json({ status: true, category });
    } else {
      response.json({ status: false, error: "Category không tồn tại" });
    }
  } else {
    response.json({ status: false, error: "Thiêu ID" });
  }
});

router.put("/", (request, response) => {
  const { ID, name } = request.body;
  console.log(request.body);
  if (ID && name) {
    categories = categories.map((cate) => {
      if (cate.ID == ID) {
        return { ...cate, name };
      }
      return cate;
    });
    response.json({ status: true });
  } else {
    response.json({ status: false, error: "Thiêu ID hoặc name" });
  }
});

module.exports = router;
