const express = require("express");
const router = express.Router();

let products = [
  { ID: 1, CATE_ID: 0, name: "Quần áo 1" },
  { ID: 2, CATE_ID: 0, name: "Quần áo 2" },
  { ID: 3, CATE_ID: 0, name: "Quần áo 3" },
  { ID: 4, CATE_ID: 0, name: "Quần áo 4" },
  { ID: 5, CATE_ID: 1, name: "Sách 1" },
  { ID: 6, CATE_ID: 1, name: "Sách 2" },
  { ID: 7, CATE_ID: 1, name: "Sách 3" },
  { ID: 8, CATE_ID: 1, name: "Sách 4" },
  { ID: 9, CATE_ID: 2, name: "Món ăn 1" },
  { ID: 10, CATE_ID: 3, name: "Hoa quả 1" },
  { ID: 11, CATE_ID: 3, name: "Hoa quả 2" },
  { ID: 12, CATE_ID: 3, name: "Hoa quả 3" },
  { ID: 13, CATE_ID: 4, name: "Giày dép 1" },
  { ID: 14, CATE_ID: 4, name: "Giày dép 2" },
  { ID: 15, CATE_ID: 4, name: "Giày dép 3" },
  { ID: 16, CATE_ID: 4, name: "Giày dép 4" },
  { ID: 17, CATE_ID: 4, name: "Giày dép 5" },
  { ID: 18, CATE_ID: 4, name: "Giày dép 6" },
  { ID: 19, CATE_ID: 4, name: "Giày dép 7" },
];

router.get("/list", async (request, response) => {
  response.json({ status: true, products });
});

router.post("/", async (request, response) => {
  const { name, CATE_ID } = request.body;
  if (name && CATE_ID) {
    const ID = products.length;
    const product = { ID, CATE_ID, name };
    products = [...products, product];
    response.json({ status: true, product });
  } else {
    response.json({ status: false, error: "Thiêu name hoặc CATE_ID" });
  }
});

router.get("/", (request, response) => {
  const { ID } = request.query;
  if (ID) {
    const product = products.find((item) => item.ID == ID);
    if (product) {
      response.json({ status: true, product });
    } else {
      response.json({ status: false, error: "product không tồn tại" });
    }
  } else {
    response.json({ status: false, error: "Thiêu ID" });
  }
});

router.put("/", (request, response) => {
  const { ID, name } = request.body;
  if (ID && name) {
    products = products.map((item) => {
      if (item.ID == ID) {
        return { ...item, name };
      }
      return item;
    });
    response.json({ status: true });
  } else {
    response.json({ status: false, error: "Thiêu ID hoặc name" });
  }
});

module.exports = router;
